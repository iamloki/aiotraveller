package com.elysium.scripts.elysiumfighter.script.core.data.travel;

import org.hexbot.api.methods.Skills;
import org.hexbot.api.methods.helper.Magic;
import org.hexbot.api.wrapper.Tile;

/**
 * @author Ben
 *         Created: 17/12/13 at 01:52.
 *         Do not distribute.
 */
public enum Teleports {

    Camelot("Camelot",new Tile(1,1), 0, new int[][]{{0}, {0}}, Magic.Spell.CAMELOT_TELEPORT, null);
    //Name, destination tile, level required, item sets required, spell (if spell, else null), interact string (if spell null)

    /**
    Reagrding item sets - each array represents all items required to cast the spell. This may be all runes.
    Or another (possibly the second) array could represent with 1 kind of staff
    A third array could represent with a different staff.
    If the teleport method is a teletab then only 1 array will be used.
    **/

    private String name;
    private Tile tile;
    private int level;
    private int[][] items;
    private Enum type;
    private String interact;

    private Teleports(String name, Tile tile, int i, int[][] items, Enum type, String interact) {
        this.name = name;
        this.tile = tile;
        this.level = i;
        this.items = items;
        this.type = type;
        this.interact = interact;
    }

    public String getName() {
        return this.name;
    }

    public Tile getTile() {
        return this.tile;
    }

    public int getLevel() {
        return this.level;
    }

    public int[][] getItemIds() {
        return this.items;
    }

    public Enum getType() {
        return this.type;
    }

    public String getInteract() {
        return this.interact;
    }
}
